# Ubuntu Conversation Challenge - Alfredo's solution

This repo contains Alfredo's solution to the Ubuntu Conversation Challenge. This README gives an overview of the solution and describes the structure of the code. This is described separately for Task 1 and Task 2.

For each notebook ending in `.ipnyb`, a static HTML version (`.html`) is provided for ease of inspection.

All code can be run, provided that all data and models are downloaded from:

https://drive.google.com/file/d/1u2kt6vqc7tFAzys7PtkReB_rfteR_3Tv/view?usp=sharing

place the `data` and `models` directory at this level.

## Task 1: Choose your agents

The full solution is given in `Task1.ipynb`. A static version of this notebook is saved as `Task1.html` which can be opened with any browser. Please refer to these files for details on the solution given.

## Task 2: Phatic-chat detector

The solution to this task is split in several notebooks starting with `Task2-*`. Again, static HTML versions for each of these notebooks is provided, for convenience of inspecting the solutions.

### Assumption

The challenge describes as phatic language, social expressions like the following:

* Greetings: hello, hi, hi there, good morning, good evening, etc.
* Farewells: bye, adios, see you later, good bye, etc.
* Pleasantries: thanks, cheers, thank you, please, etc.

I kept to this definition as much as possible when annotating the dataset. There are other expressions that arguably do not convey much meaning, such as interjections like *uh oh?*, *Umm*, *ok*, *yes* and *no*. I decided not to annotate these as phatic because they signal a response from the customer. They signal, e.g., that they are confused, that they did/did not understand the question or instruction, that they are frustrated, etc. This can be crucial information for a customer service agent. It tells them whether they are doing a good job in explaining a solution to the customer o not. Similarly, there are a lot of messages like *ok, thanks*. I did not flag these as phatic as they contain feedback/a response (*ok*). Also, there are messages (usually from customers) that aren't very useful (*help help*) or off-topic chatter (*I live in Cornwall*). I decided to not to flag these either as they don't really conform to the above definition of phatic language.

I did annotate as phatic those sentences made up entirely of smileys (*XD*, *:)*) and/or expressions like *LOL*. However, this is open to discussion as they could be interpreted as useful reactions or social signals.

### The solution

The solution itself is structured in the following notebooks (and accompanying static HTML versions):

* `Task2-1-Preprocessing`: Tokenises the messages, shuffles the chats and splits them into a training and test set.
* `Task2-2-Annotation`: Describes the annotation of the training and test sets.
* `Task2-3-LSTM-Classifier`: A classifier based on an LSTM that classifies a sentence as phatic or non-phatic.
